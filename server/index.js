const { join } = require('path');
const { performance } = require('perf_hooks');
const { renderToString } = require('react-dom/server');

const distPath = join(__dirname, '..', 'dist');
const App = require(join(distPath, 'app.node.js')).default;

performance.mark('render-start');
const appStr = renderToString(App);
performance.mark('render-end');
performance.measure('render', 'render-start', 'render-end');

const renderTime = performance.getEntriesByName('render')[0].duration;
console.log(renderTime);
