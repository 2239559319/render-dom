import React from 'react';

function App() {

    const list = new Array(100000).fill(0).map((_, i) => i + 2);

    return (
        <div className="app">
            <div>hello</div>
            <div className="list">
                {list.map((v, i) => <div key={i}>{v}</div>)}
            </div>
        </div>
    )
}

export default <App />;
