importScripts("https://unpkg.com/xiaochuan-react-develop@0.0.5/dist/18.2.0/react.production.min.js");
importScripts("https://cdn.jsdelivr.net/npm/xiaochuan-react-develop@0.0.5/dist/18.2.0/react-dom-server-legacy.browser.production.min.js");
importScripts("/app.js");

self.addEventListener('message', (event) => {
    self.clients.get(event.source.id).then((v) => {
        performance.mark('render-start');
        const str = ReactDOMServer.renderToString(self.App.default);
        performance.mark('render-end');
        performance.measure('render', 'render-start', 'render-end');

        const duration = performance.getEntriesByName('render')[0].duration;
        performance.clearMarks('render-start');
        performance.clearMarks('render-end');
        performance.clearMeasures('render');
        
        v.postMessage({
            type: 'render',
            duration,
            str,
        });
    });

});