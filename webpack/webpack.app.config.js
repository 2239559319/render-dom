const { join } = require('path');

const srcPath = join(__dirname, '..', 'src');

/**
 * @type {import('webpack').Configuration}
 */
const config1 = {
    entry: {
        app: join(srcPath, 'App.tsx'),
    },
    output: {
        filename: '[name].js',
        path: join(__dirname, '..', 'client'),
        library: {
            type: 'umd',
            name: 'App'
        }
    },
    mode: 'production',
    devtool: 'source-map',
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                use: 'ts-loader'
            }
        ]
    },
    optimization: {
        minimize: false,
    },
    externals: {
        react: 'React',
        'react-dom': 'ReactDOM'
    }
};

/**
 * @type {import('webpack').Configuration}
 */
const config2 = {
    target: 'node',
    entry: {
        app: join(srcPath, 'App.tsx'),
    },
    output: {
        filename: '[name].node.js',
        path: join(__dirname, '..', 'dist'),
        library: {
            type: 'commonjs'
        }
    },
    mode: 'production',
    devtool: 'source-map',
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                use: 'ts-loader'
            }
        ]
    },
    optimization: {
        minimize: false,
    },
    externals: {
        react: 'react',
    }
};

module.exports = [config1, config2];
